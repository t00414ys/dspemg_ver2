#
#######
#
#  libpdriver files
#

LIBRARIES	= $(LIBDIR)/libND.a

##
#
##
ifeq ($(ENVIRONMENT),RUN_ON_FRIZZ)
CFLAGS_LIB	+= -mrename-section-.text=.iram0.text
endif

##
#
##
SRCS	=	
SRCS	+=	libpdriver/src/adpd142.c
SRCS	+=	libpdriver/src/ak0991X.c
SRCS	+=	libpdriver/src/ak8963.c
#SRCS	+=	libpdriver/src/bma2xx.c
SRCS	+=	libpdriver/src/bmd101.c
SRCS	+=	libpdriver/src/gp2ap054a10f.c
SRCS	+=	libpdriver/src/gtap200.c
SRCS	+=	libpdriver/src/hsppad031.c
SRCS	+=	libpdriver/src/lt_1ph03.c
SRCS	+=	libpdriver/src/mmc3516x.c
SRCS	+=	libpdriver/src/PA12201001.c
SRCS	+=	libpdriver/src/pac7673ee.c
SRCS	+=	libpdriver/src/pah8001.c
SRCS	+=	libpdriver/src/pm131.c
SRCS	+=	libpdriver/src/stk3420.c
SRCS	+=	libpdriver/src/stk8313.c
SRCS	+=	libpdriver/src/zpa2326.c


##
#
##
INCS	=
ifneq ($(ENVIRONMENT),RUN_ON_FRIZZ)
INCS	+=	-I$(WORKSPACE_LOC)/$(STUBDIR)/include
endif
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libfrizz/inc
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libfrizz_driver/inc
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libpdriver/inc
INCS	+=	-I$(WORKSPACE_LOC)/libND/libpdriver/inc
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libpsensor/inc/config
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libpsensor/inc/drivers
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libsensors/inc
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/activity
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/libs
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/bikedetector
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/gesture
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/heartrate
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/prs
INCS	+=	-I$(WORKSPACE_LOC)/libcommon/inc
INCS	+=	-I$(WORKSPACE_LOC)/target/libcommon/inc

