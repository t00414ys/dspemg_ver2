/*!******************************************************************************
 * @file    ak8963.h
 * @brief   ak8963 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __AK8963_H__
#define __AK8963_H__
#include "ak8963_api.h"

/*
 *
 *		By environment, you must edit this file
 *
 */

/*
 * This is use magnet calibration
 *(DEFINITED) : Enable  the calibration process maker create
 *(~DEFINITED): Disable the calibration process maker create
 */
//#define	D_USE_CALIB_AK8963



/*
 *
 *		The rest of this is okay not change
 *
 */

// Constant
// Guass = 10^2*uT
// 0.15 (uT/LSB) => 0.0015 (Gauss/LSB)
#if defined(D_USE_CALIB_AK8963)
#define AK8963_MICRO_TESLA_PER_LSB		((float)(0.06f))		//16bits resolution
#else
#define AK8963_MICRO_TESLA_PER_LSB		((float)(0.15f))		//16bits resolution
//#define AK8963_MICRO_TESLA_PER_LSB		((float)(0.6f))		//14bits resolution
#endif

#define AK8963_I2C_ADDRESS_C		0x0C
#define AK8963_I2C_ADDRESS_D		0x0D
#define AK8963_I2C_ADDRESS_E		0x0E
#define AK8963_I2C_ADDRESS_F		0x0F

#define AK8963_WHOAMI_ID			0x48

#define AK8963_WIA					0x00
#define AK8963_INFO					0x01
#define AK8963_ST1					0x02
#define AK8963_HXL					0x03
#define AK8963_HXH					0x04
#define AK8963_HYL					0x05
#define AK8963_HYH					0x06
#define AK8963_HZL					0x07
#define AK8963_HZH					0x08
#define AK8963_ST2					0x09
#define AK8963_CNTL1				0x0A
#define AK8963_CNTL2				0x0B
#define AK8963_ASTC					0x0C	// Self-Test
#define AK8963_TS1					0x0D	// Prohibit
#define AK8963_TS2					0x0E	// Prohibit
#define AK8963_I2CDIS				0x0F	// I2C Disable

#define AK8963_ASAX					0x10	// Fuse ROM
#define AK8963_ASAY					0x11	// Fuse ROM
#define AK8963_ASAZ					0x12	// Fuse ROM
#define AK8963_RSV					0x13	// Prohibit

// register constant
#define AK8963_CNTL1_SNG_MEASURE			0x01
#define	AK8963_CNTL1_FUSE_ACCESS			0x0F
#define AK8963_CNTL1_POWER_DOWN				0x00
#define AK8963_CNTL1_14BITS					0x00
#define AK8963_CNTL1_16BITS					0x10
#endif
