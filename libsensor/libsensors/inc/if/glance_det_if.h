/*!******************************************************************************
 * @file    glance_det_if.h
 * @brief   virtual glance detector if sensor interface
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __GLANCE_DET_IF_H__
#define __GLANCE_DET_IF_H__

#include "libsensors_id.h"

/** @defgroup GLANCE_DET GLANCE DETECTOR
 *  @{
 */
#define GLANCE_DET_ID SENSOR_ID_GLANCE_DETECTOR	//!< An glance detector sensor interface ID

/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif
