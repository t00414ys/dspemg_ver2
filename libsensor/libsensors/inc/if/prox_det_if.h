/*!******************************************************************************
 * @file    prox_det_if.h
 * @brief   virtual proximity detector sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

#ifndef __PROX_DET_IF_H__
#define __PROX_DET_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup SENSOR_ID_PROXIMITY_DETECTOR PROXIMITY_DETECTOR
 *  @{
 */

#define PROXIMITY_DETECTOR_ID	SENSOR_ID_PROXIMITY_DETECTOR	//!< proximity detector sensor interface ID

/**
 * @struct prox_det_data_t
 * @brief Output data structure for proximity detector sensor
 */
typedef struct {
	int	data;		//!< 0x00000000 not approach , 0x00000001  approach
} prox_det_data_t;

/**
 * @name Command List
 * @note none
 */
//@{

/**
 * @brief	Set approach duration
 * @param	cmd_code	PROXIMITY_DETECTOR_CMD_SET_APPROACH_DURATION
 * @return	0:OK other:NG
 */
#define PROXIMITY_DETECTOR_CMD_SET_APPROACH_DURATION		0x00

/**
 * @brief	Set leave duration
 * @param	cmd_code	PROXIMITY_DETECTOR_CMD_SET_LEAVE_DURATION
 * @return	0:OK other:NG
 */
#define PROXIMITY_DETECTOR_CMD_SET_LEAVE_DURATION		0x01

/**
 * @brief	Set proximity high threshold
 * @param	cmd_code	PROXIMITY_DETECTOR_CMD_SET_HIGH_THD
 * @return	0:OK other:NG
 */
#define PROXIMITY_DETECTOR_CMD_SET_HIGH_THD		0x02

/**
 * @brief	Set proximity low threshold
 * @param	cmd_code	PROXIMITY_DETECTOR_CMD_SET_LOW_THD
 * @return	0:OK other:NG
 */
#define PROXIMITY_DETECTOR_CMD_SET_LOW_THD		0x03


//@}

/** @} */
#endif
