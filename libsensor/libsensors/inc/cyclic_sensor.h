/*!******************************************************************************
 * @file    cyclic_sensor.h
 * @brief   virtual cyclic sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/**
 *  @brief	Virtual cyclic timer sensor
 *  @brief	A sensor for taking out cycle.
 * - <B>Sensor ID</B> : #SENSOR_ID_CYCLIC_TIMER
 * - <B>Parent Sensor ID</B> : none
 * - <B>Detection Timing</B> : Continuous (This sensor type output sensor
 * data every priod when host determines sensor data or frizz optimized sensor data.)
 */
#ifndef __CYCLIC_SENSOR_H__
#define __CYCLIC_SENSOR_H__

#include "frizz_type.h"
#include "sensor_if.h"
#include "libsensors_id.h"

/**
 * @brief	Initialize cyclic timer sensor
 * @par		External public functions
 *
 * @retval	sensor_if_t		Sensor Interface
 */
sensor_if_t* cyclic_sensor_init( void );

#endif
