/*!******************************************************************************
 * @file    mag_hard.h
 * @brief   utility header for Hard-Iron Comp
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MAG_HARD_H__
#define __MAG_HARD_H__

#include "frizz_type.h"
#include "matrix.h"

typedef void* HardIron_h;

/**
 * @brief create HardIron
 *
 * @return object
 */
HardIron_h HardIron_New( void );

/**
 * @brief initialize HardIron
 *
 * @param [in]	h HardIron Handle
 */
void HardIron_Init( HardIron_h h );

/**
 * @brief destroy HardIron
 *
 * @param [in]	h HardIron Handle
 */
void HardIron_Delete( HardIron_h h );

/**
 * @brief set calibration data
 *
 * @param [in]	h HardIron Handle
 * @param mag[3] magnet data
 *
 * @return
 */
int HardIron_CalibPush( HardIron_h h, frizz_fp mag[3] );

/**
 * @brief calibration calculation
 *
 * @param [in]	h HardIron Handle
 * @param [out]	hard Hard-Iron Matrix: 3 x 1
 *
 * @return -1:can't calculate, 0:success
 */
int HardIron_CalibCalc( HardIron_h h, Matrix *hard );

#endif//__MAG_HARD_H__
