/*!******************************************************************************
 * @file    rot_vec.c
 * @brief   virtual rotation vector sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_const.h"
#include "sensor_if.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "quaternion_base.h"
#include "rot_vec.h"
#include "libsensors_id.h"
#include "if/rot_vec_if.h"

//#define FRIZZ_USE_LIB_POSTURE
#ifdef FRIZZ_USE_LIB_POSTURE
#include "kf_bias.h"
#endif

#define		SENSOR_VER_MAJOR		(1)						// Major Version
#define		SENSOR_VER_MINOR		(0)						// Minor Version
#define		SENSOR_VER_DETAIL		(1)						// Detail Version

#define DEF_INIT(x) x ## _init

typedef struct {
	// ID
	unsigned char		id;
	unsigned char		par_ls[3];
	// IF
	sensor_if_t			pif;
	sensor_if_get_t		*p_accl;
	sensor_if_get_t		*p_magn;
	sensor_if_get_t		*p_gyro;
	// status
	int					f_active;
	int					tick;
	int					f_need;
	int					f_start;
	unsigned int		ts;
	frizz_fp			dtm;
	// data
	__attribute__( ( aligned( 16 ) ) ) rotation_vector_data_t	data;
#ifdef FRIZZ_USE_LIB_POSTURE
	KF_Bias_h			kf;
#else
	frizz_fp4w_t		q;
#endif
} device_sensor_t;

static device_sensor_t g_device;

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	*list = g_device.par_ls;
	return sizeof( g_device.par_ls );
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = &g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return sizeof( rotation_vector_data_t ) / sizeof( int );
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
	if( gettor->id() == SENSOR_ID_ACCEL_RAW ) {
		g_device.p_accl = gettor;
	}
	if( gettor->id() == SENSOR_ID_MAGNET_CALIB_HARD ) {
		g_device.p_magn = gettor;
	}
	if( gettor->id() == SENSOR_ID_GYRO_RAW ) {
		g_device.p_gyro = gettor;
	}
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		g_device.f_active = f_active;
		hub_mgr_set_sensor_interval( g_device.id, g_device.p_accl->id(), g_device.tick );
		hub_mgr_set_sensor_interval( g_device.id, g_device.p_magn->id(), g_device.tick );
		hub_mgr_set_sensor_interval( g_device.id, g_device.p_gyro->id(), g_device.tick );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_accl->id(), g_device.f_active );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_magn->id(), g_device.f_active );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_gyro->id(), g_device.f_active );
		g_device.f_start = f_active;
	}
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	return g_device.tick;
}

static int get_interval( void )
{
	return g_device.tick;
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION:
		ret =	make_version( SENSOR_VER_MAJOR, SENSOR_VER_MINOR, SENSOR_VER_DETAIL );
		break;
	default:
		ret = RESULT_ERR_CMD;
	}
	return ret;
}

static unsigned int notify_ts( unsigned int ts )
{
	g_device.ts = ts;
	g_device.f_need = 1;
	return ts + g_device.tick;
}

static int calculate( void )
{
	frizz_fp4w *accl, *magn, *gyro;
	g_device.p_accl->data( ( void** )&accl, &g_device.ts );
	g_device.p_magn->data( ( void** )&magn, &g_device.ts );
	g_device.p_gyro->data( ( void** )&gyro, &g_device.ts );
	// set
	if( g_device.f_start ) {
#ifdef FRIZZ_USE_LIB_POSTURE
		KF_BiasInit( g_device.kf );
		KF_BiasSetState( g_device.kf, 0, gyro );
		KF_BiasSetPosture( g_device.kf, accl, magn );
#else
		quaternion_base_am_generate( &g_device.q, accl, magn );
#endif
		g_device.f_start = 0;
	} else {
#ifdef FRIZZ_USE_LIB_POSTURE
		KF_BiasPredict( g_device.kf, gyro, g_device.dtm );
		KF_BiasMeasure( g_device.kf, accl, magn );
		KF_BiasFiltering( g_device.kf );
#else
		quaternion_base_amg_update( &g_device.q, accl, magn, gyro, g_device.dtm );
#endif
	}
#ifdef FRIZZ_USE_LIB_POSTURE
	{
		const frizz_fp4w_t *q = KF_BiasGetQuaternion( g_device.kf );
		g_device.data.data[0] = q->z[0];
		g_device.data.data[1] = q->z[1];
		g_device.data.data[2] = q->z[2];
		g_device.data.data[3] = q->z[3];
	}
#else
	g_device.data.data[0] = g_device.q.z[0];
	g_device.data.data[1] = g_device.q.z[1];
	g_device.data.data[2] = g_device.q.z[2];
	g_device.data.data[3] = g_device.q.z[3];
#endif
	// clear
	g_device.f_need = 0;
	return 1;
}

static void end( void )
{
#ifdef FRIZZ_USE_LIB_POSTURE
	KF_BiasDelete( g_device.kf );
#endif
}

sensor_if_t* DEF_INIT( rot_vec )( void )
{
	// ID
	g_device.id = SENSOR_ID_ROTATION_VECTOR;
	g_device.par_ls[0] = SENSOR_ID_ACCEL_RAW;
	g_device.par_ls[1] = SENSOR_ID_MAGNET_CALIB_HARD;
	g_device.par_ls[2] = SENSOR_ID_GYRO_RAW;
	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = 0;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	g_device.pif.end = end;
	// param
	g_device.f_active = 0;
	g_device.tick = 10;
	g_device.f_need = 0;
	g_device.f_start = 0;
	g_device.ts = 0;
	g_device.dtm = as_frizz_fp( 0.01f );
	// quaternion + accuracy
	g_device.data.accr = as_frizz_fp( 0.05235987755982988730771072305466 );	/// 3.0 degree

#ifdef FRIZZ_USE_LIB_POSTURE
	g_device.kf = KF_BiasNew();
#endif

	return &( g_device.pif );
}

