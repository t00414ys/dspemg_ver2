/*!******************************************************************************
 * @file    prox_driver.h
 * @brief   sample program for proximity sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

#ifndef __PROX_DRIVER_H__
#define __PROX_DRIVER_H__

#include "config.h"
#include "base_driver.h"

#if defined(USE_STK3420)
#include "stk3420_api.h"
#define	STK3420_DATA		{	stk3420_init,						stk3420_ctrl_prox,			stk3420_rcv_prox ,			stk3420_conv_prox, 		\
								stk3420_setparam,					stk3420_get_ver,			stk3420_get_name ,			0,						\
								{stk3420_prox_get_condition,		0,							0,							0}						},
#else
#define	STK3420_DATA
#endif

#if defined(USE_GP2AP054A10F)
#include "gp2ap054a10f_api.h"
#define GP2AP054A10F_DATA	{	gp2ap054a10f_init,					gp2ap054a10f_ctrl_prox,		gp2ap054a10f_rcv_prox,		gp2ap054a10f_conv_prox,	\
								gp2ap054a10f_setparam,				gp2ap054a10f_get_ver,		gp2ap054a10f_get_name,		0,						\
								{gp2ap054a10f_prox_get_condition,	0,							0,							0}						},
#else
#define GP2AP054A10F_DATA
#endif

#if defined(USE_PAC7673EE)
#include "pac7673ee_api.h"
#define	PAC7673EE_DATA		{	pac7673ee_init,						pac7673ee_ctrl_prox,			pac7673ee_rcv_prox ,			pac7673ee_conv_prox, 		\
								pac7673ee_setparam,					pac7673ee_get_ver,			pac7673ee_get_name ,			0,						\
								{pac7673ee_prox_get_condition,		0,							0,							0}						},
#else
#define	PAC7673EE_DATA
#endif

#if defined(USE_PA12201001)
#include "pa12201001_api.h"
#define	PA12201001_DATA		{	pa12201001_init,						pa12201001_ctrl_prox,			pa12201001_rcv_prox ,			pa12201001_conv_prox, 		\
								pa12201001_setparam,					pa12201001_get_ver,			pa12201001_get_name ,			0,						\
								{pa12201001_prox_get_condition,		0,							0,							0}						},
#else
#define	PA12201001_DATA
#endif

#if defined(USE_PROX_EMU)
#include "proxemu_api.h"
#define	PROXEMU_DATA		{	proxemu_init,					proxemu_ctrl,				proxemu_rcv ,				proxemu_conv, 		\
								proxemu_setparam,				proxemu_get_ver,			proxemu_get_name ,			0,					\
								{proxemu_get_condition,			0,							0,							0}					},
#else
#define	PROXEMU_DATA
#endif

#endif /*  */
