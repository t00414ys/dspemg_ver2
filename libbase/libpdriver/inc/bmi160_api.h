/*!******************************************************************************
 * @file    bmi160_api.h
 * @brief   bmi160 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __BMI160_API_H__
#define __BMI160_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int bmi160_init( unsigned int param );
void bmi160_ctrl_accl( int f_ena );
void bmi160_ctrl_gyro( int f_ena );
unsigned int bmi160_rcv_accl( unsigned int tick );
unsigned int bmi160_rcv_gyro( unsigned int tick );
int bmi160_conv_accl( frizz_fp data[3] );
int bmi160_conv_gyro( frizz_fp data[4] );

int bmi160_setparam_accl( void *ptr );
int bmi160_setparam_gyro( void *ptr );
unsigned int bmi160_get_ver( void );
unsigned int bmi160_get_name( void );

int bmi160_accl_get_condition( void *data );
int bmi160_gyro_get_condition( void *data );
int bmi160_accl_get_raw_data( void *data );

#ifdef __cplusplus
}
#endif

#endif
