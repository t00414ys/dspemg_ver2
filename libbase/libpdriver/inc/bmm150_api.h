/*!******************************************************************************
 * @file    bmm150_api.h
 * @brief   bmm150 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __BMM150_API_H__
#define __BMM150_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int bmm150_init( unsigned int param );
void bmm150_ctrl( int f_ena );
unsigned int bmm150_rcv( unsigned int tick );
int bmm150_conv( frizz_fp data[3] );

int bmm150_setparam( void *buffer );
unsigned int bmm150_get_ver( void );
unsigned int bmm150_get_name( void );

int bmm150_calib_get_status( void *result );
int bmm150_calib_get_data( void *data );
int bmm150_calib_set_data( void *data );

int bmm150_get_condition( void *data );

#ifdef __cplusplus
}
#endif

#endif
