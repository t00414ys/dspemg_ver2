/*!******************************************************************************
 * @file    bmm150.h
 * @brief   bmm150 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __BMM150_H__
#define __BMM150_H__
#include "bmm150_api.h"

/*
 *
 *		By environment, you must edit this file
 *
 */

/*
 * This is use magnet calibration
 *(DEFINITED) : Enable  the calibration process maker create
 *(~DEFINITED): Disable the calibration process maker create
 */
//#define	D_USE_CALIB_BMM150



/*
 *
 *		The rest of this is okay not change
 *
 */

#define CTRL_ACTIVATE					(1)
#define CTRL_DEACTIVATE					(0)

#define BMM150_MICRO_TESLA_PER_LSB		(1./16.)
/*
 * CSB : SDO = GND : GND => 0x10
 * CSB : SDO = GND : VDD => 0x11
 * CSB : SDO = VDD : GND => 0x12
 * CSB : SDO = VDD : VDD => 0x13
 */
#define BMM150_I2C_ADDRESS_10				(0x10)		///< Magnet Sensor(made from BMM)
#define BMM150_I2C_ADDRESS_11				(0x11)		///< Magnet Sensor(made from BMM)
#define BMM150_I2C_ADDRESS_12				(0x12)		///< Magnet Sensor(made from BMM)
#define BMM150_I2C_ADDRESS_13				(0x13)		///< Magnet Sensor(made from BMM)

/********************************************/
/**\name	CHIP ID       */
/********************************************/
/* Fixed Data Registers */
#define BMM150_CHIP_ID                     0x40
/********************************************/
/**\name	DATA REGISTERS       */
/********************************************/
/* Data Registers*/
#define BMM150_DATAX_LSB                   0x42
#define BMM150_DATAX_MSB                   0x43
#define BMM150_DATAY_LSB                   0x44
#define BMM150_DATAY_MSB                   0x45
#define BMM150_DATAZ_LSB                   0x46
#define BMM150_DATAZ_MSB                   0x47
#define BMM150_R_LSB                       0x48
#define BMM150_R_MSB                       0x49

/********************************************/
/**\name	POWER MODE DEFINITIONS      */
/********************************************/
/* Control Registers */
#define BMM150_POWER_CONTROL               0x4B
#define BMM150_CONTROL                     0x4C
#define BMM150_INT_CONTROL                 0x4D
#define BMM150_SENS_CONTROL                0x4E
#define BMM150_LOW_THRES                   0x4F
#define BMM150_HIGH_THRES                  0x50

/********************************************/
/**\name XY AND Z REPETITIONS DEFINITIONS  */
/********************************************/
#define BMM150_REP_XY                      0x51
#define BMM150_REP_Z                       0x52

/********************************************/
/**\name	TRIM REGISTERS      */
/********************************************/
/* Trim Extended Registers */
#define BMM150_DIG_X1                      0x5D
#define BMM150_DIG_Y1                      0x5E
#define BMM150_DIG_Z4_LSB                  0x62
#define BMM150_DIG_Z4_MSB                  0x63
#define BMM150_DIG_X2                      0x64
#define BMM150_DIG_Y2                      0x65
#define BMM150_DIG_Z2_LSB                  0x68
#define BMM150_DIG_Z2_MSB                  0x69
#define BMM150_DIG_Z1_LSB                  0x6A
#define BMM150_DIG_Z1_MSB                  0x6B
#define BMM150_DIG_XYZ1_LSB                0x6C
#define BMM150_DIG_XYZ1_MSB                0x6D
#define BMM150_DIG_Z3_LSB                  0x6E
#define BMM150_DIG_Z3_MSB                  0x6F
#define BMM150_DIG_XY2                     0x70
#define BMM150_DIG_XY1                     0x71

// constant
#define BMM150_WHOAMI_ID				(0x32)
#define BMM150_POWER_SLEEP				(0x01)

/********************************************/
/**\name POWER MODE DEFINITIONS  */
/********************************************/
#define BMM150_NOMAL_MODE				(0x00<<1)
#define BMM150_FORCED_MODE				(0x01<<1)
#define BMM150_SLEEP_MODE				(0x03<<1)

/* Data Rates */
#define	BMM150_DR_10HZ					(0x0<<3)
#define	BMM150_DR_2HZ					(0x1<<3)
#define	BMM150_DR_6HZ					(0x2<<3)
#define	BMM150_DR_8HZ					(0x3<<3)
#define	BMM150_DR_15HZ					(0x4<<3)
#define	BMM150_DR_20HZ					(0x5<<3)
#define	BMM150_DR_25HZ					(0x6<<3)
#define	BMM150_DR_30HZ					(0x7<<3)

/* PRESET MODES - DATA RATES */
#define BMM150_LOWPOWER_DR                       BMM150_DR_10HZ
#define BMM150_REGULAR_DR                        BMM150_DR_10HZ
#define BMM150_HIGHACCURACY_DR                   BMM150_DR_20HZ
#define BMM150_ENHANCED_DR                       BMM150_DR_10HZ

/* PRESET MODES - REPETITIONS-XY RATES */
#define BMM150_LOWPOWER_REPXY                     1
#define BMM150_REGULAR_REPXY                      4
#define BMM150_HIGHACCURACY_REPXY                23
#define BMM150_ENHANCED_REPXY                     7

/* PRESET MODES - REPETITIONS-Z RATES */
#define BMM150_LOWPOWER_REPZ                      2
#define BMM150_REGULAR_REPZ                      14
#define BMM150_HIGHACCURACY_REPZ                 82
#define BMM150_ENHANCED_REPZ                     26

/****************************************************/
/**\name	COMPENSATED FORMULA DEFINITIONS      */
/***************************************************/
#define BMM050_HEX_FOUR_THOUSAND			0x4000
#define BMM050_HEX_ONE_LACK					0x100000
#define BMM050_HEX_A_ZERO					0xA0

/********************************************/
/**\name NUMERIC DEFINITIONS  */
/********************************************/
#define         C_BMM050_ZERO_U8X				0
#define         C_BMM050_ONE_U8X				1
#define         C_BMM050_TWO_U8X				2
#define         C_BMM050_FOUR_U8X				4
#define         C_BMM050_FIVE_U8X				5
#define         C_BMM050_EIGHT_U8X				8

#define BMM050_ZERO_U8X                         0
#define BMM050_NEGATIVE_SATURATION_Z            -32767
#define BMM050_POSITIVE_SATURATION_Z            32767

#endif
