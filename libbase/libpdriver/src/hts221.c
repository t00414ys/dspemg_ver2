/*!******************************************************************************
 * @file    hts221.c
 * @brief   hts221 sensor driver
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "hts221.h"
#include "config_type.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_HTS221


#define		DRIVER_VER_MAJOR		(1)						// Major Version
#define		DRIVER_VER_MINOR		(1)						// Minor Version
#define		DRIVER_VER_DETAIL		(0)						// Detail Version


struct {
	frizz_fp			scale;		// scaler
	unsigned char		buff[2];	// transmission buffer,
	int                 H_T_OUT[2];
	unsigned short      H_RH[2];
	unsigned char		addr;		// detect automatically
	unsigned char		device_id;
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[1];
	//
} g_humidity_hts221;

#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char		g_init_done = D_INIT_NONE;

int hts221_init( unsigned int param )
{
	int ret;
	unsigned char buff;
	unsigned char data;

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	// Get Device ID
	g_humidity_hts221.addr = PRES_hts221_I2C_ADDRESS;

	buff = 0;
	i2c_read( g_humidity_hts221.addr, HTS221_WHO_AM_I , &buff, 1 );	// Device ID of HTS2
	if( buff != HTS221_DEVICE_ID ) {
		return RESULT_ERR_INIT;
	}

	////////////////////
	//
	// read output data rate & power on-off mode
	g_humidity_hts221.device_id = 0;
	i2c_read( g_humidity_hts221.addr, HTS221_CTRL_REG1 , &g_humidity_hts221.device_id, 1 );	// output data rate
	if( g_humidity_hts221.device_id != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	buff  = buff & 0x78;
	buff  = buff | ( HTS221_FREQ_ONESHOT << HTS221_ODR_BIT );			// init Hz
	buff  = buff | ( 0x01 << HTS221_BDU );			// 1: output registers not updated until MSB and LSB have been read)
	data  = buff | ( 0x00 << HTS221_POWER_DOWN );				// disable first.
	ret = i2c_write( g_humidity_hts221.addr, HTS221_CTRL_REG1 , &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	ret = i2c_read( g_humidity_hts221.addr, HTS221_H0_T0_OUT | HTS221_I2C_BURSTREAD , &g_humidity_hts221.buff[0], 2 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	g_humidity_hts221.H_T_OUT[0] = ( ( ( signed int ) g_humidity_hts221.buff[1] << 8 ) |
									 ( ( signed int ) g_humidity_hts221.buff[0] <<  0 ) );

	ret = i2c_read( g_humidity_hts221.addr, HTS221_H1_T0_OUT | HTS221_I2C_BURSTREAD , &g_humidity_hts221.buff[0], 2 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	g_humidity_hts221.H_T_OUT[1] = ( ( ( signed int ) g_humidity_hts221.buff[1] << 8 ) |
									 ( ( signed int ) g_humidity_hts221.buff[0] <<  0 ) );

	ret = i2c_read( g_humidity_hts221.addr, HTS221_H0_rH_x2 , &g_humidity_hts221.buff[0], 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	g_humidity_hts221.H_RH[0] = ( ( unsigned short ) g_humidity_hts221.buff[0] ) >> 1;

	ret = i2c_read( g_humidity_hts221.addr, HTS221_H1_rH_x2 , &g_humidity_hts221.buff[0], 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	g_humidity_hts221.H_RH[1] = ( ( unsigned short ) g_humidity_hts221.buff[0] ) >> 1;

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}


void hts221_ctrl( int f_ena )
{
	unsigned char buff;

	buff = 0;
	i2c_read( g_humidity_hts221.addr, HTS221_CTRL_REG1 , &buff, 1 );	// activeMode

	buff      = buff & 0x7f;
	if( f_ena ) {
		buff = buff | ( 0x01 << HTS221_POWER_DOWN );  	//enable Humidity sensor
	} else {
		buff = buff | ( 0x00 << HTS221_POWER_DOWN );  	//disable Humidity sensor
	}

	i2c_write( g_humidity_hts221.addr, HTS221_CTRL_REG1 , &buff, 1 );	// activeMode
}

unsigned int hts221_rcv( unsigned int tick )
{
	unsigned char buff;
	i2c_read( g_humidity_hts221.addr, HTS221_CTRL_REG1 , &buff, 1 );
	buff &= HTS221_ODR_MASK;
	if( buff == 0 ) {
		i2c_read( g_humidity_hts221.addr, HTS221_CTRL_REG2 , &buff, 1 );
		buff &= ~( HTS221_ONE_SHOT_MASK );
		buff |= HTS221_ONE_SHOT_MASK;
		i2c_write( g_humidity_hts221.addr, HTS221_CTRL_REG2 , &buff, 1 );
	}
	/*
		do{
		    i2c_read(g_humidity_hts221.addr, HTS221_STATUS_REG , &buff, 1);
		}while(!(buff&&0x02));
	*/
	// humidity 2byte
	g_humidity_hts221.recv_result = i2c_read( g_humidity_hts221.addr, HTS221_HUMIDITY_OUT_L | HTS221_I2C_BURSTREAD , &g_humidity_hts221.buff[0], 2 );
	if( g_humidity_hts221.recv_result == 0 ) {
		g_humidity_hts221.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_humidity_hts221.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}
	i2c_read( g_humidity_hts221.addr, HTS221_STATUS_REG , &buff, 1 );

	return 0;
}

int hts221_conv( frizz_fp* humid_data )
{
	frizz_fp fz;
	float* fp = ( float* )&fz;
	int sensor_buff;

	if( g_humidity_hts221.recv_result != 0 ) {
		*humid_data = g_humidity_hts221.lasttime_data[0];
		return RESULT_SUCCESS_CONV;
	}
	sensor_buff = ( ( ( unsigned int ) g_humidity_hts221.buff[1] << 8 ) |
					( ( unsigned int ) g_humidity_hts221.buff[0] <<  0 ) );

	fp[0] = ( ( float )( sensor_buff - g_humidity_hts221.H_T_OUT[0] ) ) / ( g_humidity_hts221.H_T_OUT[1] - g_humidity_hts221.H_T_OUT[0] ) * ( g_humidity_hts221.H_RH[1] - g_humidity_hts221.H_RH[0] ) + g_humidity_hts221.H_RH[0];

	*humid_data = fz;

	g_humidity_hts221.lasttime_data[0] = *humid_data;

	return RESULT_SUCCESS_CONV;
}


int hts221_setparam( void *ptr )
{
	return RESULT_ERR_SET;
}


int hts221_get_condition( void *data )
{
	return g_humidity_hts221.device_condition;
}


unsigned int hts221_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int hts221_get_name()
{
	return	D_DRIVER_NAME;
}

