/*!******************************************************************************
 * @file    raw mock.c
 * @brief   Processing to simulate various sensors
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <string.h>
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "frizz_util.h"
#include "config_type.h"
#include "raw_mock_api.h"


#define		D_DRIVER_NAME			D_DRIVER_NAME_RAWMOCK

#define		DRIVER_VER_MAJOR		(1)						// Major Version
#define		DRIVER_VER_MINOR		(0)						// Minor Version
#define		DRIVER_VER_DETAIL		(0)						// Detail Version


int raw_mock_init( unsigned int param )
{
	return RESULT_SUCCESS_INIT;
}

void raw_mock_ctrl( int f_ena )
{
	return;
}

unsigned int raw_mock_rcv( unsigned int tick )
{
	return 0;
}

int raw_mock_conv( frizz_fp *data )
{
	return RESULT_SUCCESS_CONV;
}


int raw_mock_setparam( void *ptr )
{
	return RESULT_SUCCESS_SET;
}

int raw_mock_get_condition( void *data )
{
	return D_RAW_DEVICE_ERR_READ;
}


unsigned int raw_mock_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int raw_mock_get_name()
{
	return	D_DRIVER_NAME;
}

