#######
#
#  libfrizz files
#

LIBRARIES	= $(LIBDIR)/libbase.a

##
#
##
SRCS	=
SRCS	+=	libfrizz/src/math/frizz_acos.c
SRCS	+=	libfrizz/src/math/frizz_asin.c
SRCS	+=	libfrizz/src/math/frizz_asinef.c
SRCS	+=	libfrizz/src/math/frizz_atan.c
SRCS	+=	libfrizz/src/math/frizz_atan2.c
SRCS	+=	libfrizz/src/math/frizz_cos.c
SRCS	+=	libfrizz/src/math/frizz_div.c
SRCS	+=	libfrizz/src/math/frizz_fabs.c
SRCS	+=	libfrizz/src/math/frizz_frexp.c
SRCS	+=	libfrizz/src/math/frizz_inv_sqrt.c
SRCS	+=	libfrizz/src/math/frizz_log.c
SRCS	+=	libfrizz/src/math/frizz_math_common.c
SRCS	+=	libfrizz/src/math/frizz_range.c
SRCS	+=	libfrizz/src/math/frizz_sin.c
SRCS	+=	libfrizz/src/math/frizz_sinef.c
SRCS	+=	libfrizz/src/math/frizz_sqrt.c
SRCS	+=	libfrizz/src/emutie.c
SRCS	+=	libfrizz/src/frizz_const.c
SRCS	+=	libfrizz/src/frizz_fft.c
SRCS	+=	libfrizz/src/frizz_mem.c
SRCS	+=	libfrizz/src/halffloat.c
SRCS	+=	libfrizz/src/kalman_filter.c
SRCS	+=	libfrizz/src/matrix.c
SRCS	+=	libfrizz/src/sensor_statistics.c
SRCS	+=	libfrizz/src/mcc_mem.c
SRCS	+=	libfrizz/src/quaternion_base.c
ifeq ($(ENVIRONMENT),RUN_ON_FRIZZ)
SRCS	+=	libfrizz_driver/src/counter/counter.c
SRCS	+=	libfrizz_driver/src/gpio/gpio.c
SRCS	+=	libfrizz_driver/src/i2c/i2c.c
SRCS	+=	libfrizz_driver/src/inout/cmd_in.c
SRCS	+=	libfrizz_driver/src/inout/data_out.c
SRCS	+=	libfrizz_driver/src/mes/mes.c
SRCS	+=	libfrizz_driver/src/quart/quart.c
SRCS	+=	libfrizz_driver/src/quart/tyny_sprintf.c
SRCS	+=	libfrizz_driver/src/spi/spi.c
SRCS	+=	libfrizz_driver/src/timer/timer.c
SRCS	+=	libfrizz_driver/src/uart/uart.c
endif
SRCS	+=	libfrizz_hubhal/src/hubhal_block.c
SRCS	+=	libfrizz_hubhal/src/hubhal_clk_calibrate.c
SRCS	+=	libfrizz_hubhal/src/hubhal_file_api.c
SRCS	+=	libfrizz_hubhal/src/hubhal_host_api.c
SRCS	+=	libfrizz_hubhal/src/hubhal_in.c
SRCS	+=	libfrizz_hubhal/src/hubhal_out.c
SRCS	+=	libfrizz_hubhal/src/hubhal_out_txt.c
SRCS	+=	libhub/src/fifo_mgr.c
SRCS	+=	libhub/src/frizz_debug.c
SRCS	+=	libhub/src/hub_mgr.c
SRCS	+=	libhub/src/hubmgr_dev.c
SRCS	+=	libhub/src/sensor_list.c
SRCS	+=	libpdriver/src/adxl362.c
SRCS	+=	libpdriver/src/bma2xx.c
SRCS	+=	libpdriver/src/bmi160.c
SRCS	+=	libpdriver/src/bmm150.c
SRCS	+=	libpdriver/src/bmp280.c
SRCS	+=	libpdriver/src/hp203b.c
SRCS	+=	libpdriver/src/hts221.c
SRCS	+=	libpdriver/src/hy3118.c
SRCS	+=	libpdriver/src/lis2dh.c
SRCS	+=	libpdriver/src/lis2ds12.c
SRCS	+=	libpdriver/src/lps25h.c
SRCS	+=	libpdriver/src/lsm330.c
SRCS	+=	libpdriver/src/lsm6ds3.c
SRCS	+=	libpdriver/src/mc3413.c
SRCS	+=	libpdriver/src/mpl115a2.c
SRCS	+=	libpdriver/src/mpuxxxx.c
SRCS	+=	libpdriver/src/raw_mock.c
SRCS	+=	libpdriver/src/ssd1306.c



##
#
##
INCS	=
ifneq ($(ENVIRONMENT),RUN_ON_FRIZZ)
INCS	+=	-I$(WORKSPACE_LOC)/$(STUBDIR)/include
endif
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libfrizz/inc
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libhub/inc
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libfrizz_driver/inc
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libsensors/inc
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libpsensor/inc/config
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libpsensor/inc/drivers
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libfrizz_hubhal/inc
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libsensors/inc
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libfrizz_hubhal/inc
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libpdriver/inc
INCS	+=	-I$(WORKSPACE_LOC)/libcommon/inc
INCS	+=	-I$(WORKSPACE_LOC)/target/libcommon/inc

