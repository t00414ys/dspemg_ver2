/*!******************************************************************************
 * @file frizz_peri.h
 * @brief for frizz peripheral driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_PERI_H__
#define __FRIZZ_PERI_H__

extern	unsigned int	g_ROSC1_FREQ;
extern	unsigned int	g_ROSC2_FREQ;
#ifndef	RUN_ON_PC_EXEC
// Base Address
/* IRAM */
#define BASEADR_IRAM 	0x60060000
#define BASEADR_IRAM0 	0x60060000
#define BASEADR_IRAM1 	0x60080000
/* DRAM */
#define BASEADR_DRAM 	0x60020000
#define BASEADR_DRAM0 	0x60020000
#define BASEADR_DRAM1 	0x60040000
/* Peripheral on XLMI */
#define BASEADR_XLMI  	0x6001C000

/* Util */
// Define
#define DEF_XLMI_MAPPING(addr)	((volatile unsigned short*)(BASEADR_XLMI + addr))
// Register Utility
#define XLMI_WR(_XLMIWR_ADR_ , _XLMIWR_DATA_) \
	(*(volatile unsigned short *)(_XLMIWR_ADR_)) = ((volatile unsigned short)(_XLMIWR_DATA_))
#define XLMI_RD(_XLMIRD_ADR_ ) \
	(*(volatile unsigned short *)(_XLMIRD_ADR_))


// I2C Master Block
#define REGI2C_PRER			DEF_XLMI_MAPPING(0x0200)
#define REGI2C_CTR			DEF_XLMI_MAPPING(0x0220)
#define REGI2C_TXR			DEF_XLMI_MAPPING(0x0230)
#define REGI2C_RXR			DEF_XLMI_MAPPING(0x0230)
#define REGI2C_CR			DEF_XLMI_MAPPING(0x0240)
#define REGI2C_SR			DEF_XLMI_MAPPING(0x0240)
// Message Register
#define REGMES_L			DEF_XLMI_MAPPING(0x0280)
#define REGMES_H			DEF_XLMI_MAPPING(0x0290)
// SPI Master Block
#define REGSPI_CTRL0		DEF_XLMI_MAPPING(0x0300)
#define REGSPI_CTRL1		DEF_XLMI_MAPPING(0x0310)
#define REGSPI_CLKGENH		DEF_XLMI_MAPPING(0x0320)
#define REGSPI_CLKGENL		DEF_XLMI_MAPPING(0x0330)
#define REGSPI_TXREG		DEF_XLMI_MAPPING(0x0340)
#define REGSPI_RXREG		DEF_XLMI_MAPPING(0x0350)
#define REGSPI_INTSPIM		DEF_XLMI_MAPPING(0x0360)

// UART Block
#define REGUART_DL			DEF_XLMI_MAPPING(0x0400)
#define REGUART_RB			DEF_XLMI_MAPPING(0x0400)
#define REGUART_TR			DEF_XLMI_MAPPING(0x0400)
#define REGUART_IE			DEF_XLMI_MAPPING(0x0410)
#define REGUART_II			DEF_XLMI_MAPPING(0x0420)
#define REGUART_LC			DEF_XLMI_MAPPING(0x0430)
#define REGUART_MC			DEF_XLMI_MAPPING(0x0440)
#define REGUART_LS			DEF_XLMI_MAPPING(0x0450)
#define REGUART_FC			DEF_XLMI_MAPPING(0x0450)
#define REGUART_TC			DEF_XLMI_MAPPING(0x0460)
#define REGUART_RC			DEF_XLMI_MAPPING(0x0470)
#define REGUART_TF			DEF_XLMI_MAPPING(0x0480)
#define REGUART_RF			DEF_XLMI_MAPPING(0x0490)
// GPIO Block
#define REGGPIO_MD			DEF_XLMI_MAPPING(0x0500)
#define REGGPIO_ST			DEF_XLMI_MAPPING(0x0510)

// PWM Block
#define REGPWM0_C0			DEF_XLMI_MAPPING(0x0600)
#define REGPWM0_C1			DEF_XLMI_MAPPING(0x0610)
#define REGPWM0_D0			DEF_XLMI_MAPPING(0x0620)
#define REGPWM1_C0			DEF_XLMI_MAPPING(0x0700)
#define REGPWM1_C1			DEF_XLMI_MAPPING(0x0710)
#define REGPWM1_D0			DEF_XLMI_MAPPING(0x0720)

// Awake interrupt
#define REGAWAKE0			DEF_XLMI_MAPPING(0x0540)
#define REGAWAKE1			DEF_XLMI_MAPPING(0x0550)
#define REGAWAKEM			DEF_XLMI_MAPPING(0x0560)

// ALT func
#define REGALT_FUNC			DEF_XLMI_MAPPING(0x05A0)

// FIFO
#define REGFIFO_ERR			DEF_XLMI_MAPPING(0x05C0)
#define REGFIFO_CNT			DEF_XLMI_MAPPING(0x05D0)
// Version
#define REGVER				DEF_XLMI_MAPPING(0x05E0)
#define REGXMODE0			DEF_XLMI_MAPPING(0x05F0)
#define REGXMODE1			DEF_XLMI_MAPPING(0x05B0)

#endif	// #ifndef RUN_ON_PC_EXEC

// Interrupt No.
typedef enum {
	INTERRUPT_NO_TIMER0 = 0,
	INTERRUPT_NO_TIMER1,
	INTERRUPT_NO_MESSAGE,
	INTERRUPT_NO_TIMER2,
	INTERRUPT_NO_AWAKE,
	INTERRUPT_NO_I2C,
	INTERRUPT_NO_UART,
	INTERRUPT_NO_GPIO3,
	INTERRUPT_NO_8_NOTUSE,
	INTERRUPT_NO_SPI,
	INTERRUPT_NO_GPIO2,
	INTERRUPT_NO_11_NOTUSE,
	INTERRUPT_NO_12_NOTUSE,
	INTERRUPT_NO_FIFO_RD,
	INTERRUPT_NO_GPIO3_EDGE,
	INTERRUPT_NO_GPIO2_EDGE,
	INTERRUPT_NO_DELIMITER
} eINT_NO;


// Interrupt Level
#define INTERRUPT_LEVEL_TIMER0		1
#define INTERRUPT_LEVEL_TIMER1		2
#define INTERRUPT_LEVEL_MESSAGE		4
#define INTERRUPT_LEVEL_TIMER2		3
#define INTERRUPT_LEVEL_AWAKE		4
#define INTERRUPT_LEVEL_I2C			1
#define INTERRUPT_LEVEL_UART		1
#define INTERRUPT_LEVEL_GPIO3		1
#define INTERRUPT_LEVEL_SPI			1
#define INTERRUPT_LEVEL_GPIO2		1
#define INTERRUPT_LEVEL_FIFO_RD		1
#define INTERRUPT_LEVEL_GPIO3_EDGE	1
#define INTERRUPT_LEVEL_GPIO2_EDGE	1

#define INTERRUPT_LEVEL_EXP			5

#endif//__FRIZZ_PERI_H__
