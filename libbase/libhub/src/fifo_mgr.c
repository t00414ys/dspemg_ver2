/*!******************************************************************************
 * @file   fifo_mgr.c
 * @brief  software fifo manager
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
//#define _DEBUG

#ifdef RUN_ON_PC
#include <stdio.h>
#endif

#include "fifo_mgr.h"

#define SET_HEADER(id, num) (((id)<<16) | ((num) & 0xFFFF))
#define GET_ID(header) (((header)>>16)&0xFF)
#define GET_NUM(header) ((header)&0xFFFF)

static int peek( fifo_mgr_t* o, unsigned char *sensor_id, unsigned int *ts, void* p, unsigned int *size )
{
	if( 0 < o->num ) {
		unsigned int *data = ( unsigned int* )p;
		unsigned int *br_p = o->r_p;
		unsigned int num, i;
		// get num
		num = GET_NUM( *( o->r_p ) );
		if( size != 0 ) {
			*size = num;
		}
		// get sensor's ID
		if( sensor_id != 0 ) {
			*sensor_id = GET_ID( *( o->r_p ) );
		}
		o->r_p++;
		if( o->end <= o->r_p ) {
			o->r_p = o->top + ( o->r_p - o->end );
		}
		if( ts != 0 ) {
			*ts = *( o->r_p );
		}
		o->r_p++;
		if( o->end <= o->r_p ) {
			o->r_p = o->top + ( o->r_p - o->end );
		}
		for( i = 0; i < num; i++ ) {
			if( p != 0 ) {
				*data = *( o->r_p );
			}
			o->r_p++;
			data++;
			if( o->end <= o->r_p ) {
				o->r_p = o->top + ( o->r_p - o->end );
			}
		}
		o->r_p = br_p;
		return 0;
	} else {
		return -1;
	}
}

static int drop( fifo_mgr_t* o )
{
	if( 0 < o->num ) {
		// get num
		o->r_p += 2 + GET_NUM( *( o->r_p ) );	// header + ts + data
		if( o->end <= o->r_p ) {
			o->r_p = o->top + ( o->r_p - o->end );
		}
		o->num--;
		return 0;
	} else {
		return -1;
	}
}

/**
 * @brief Init FIFO Table
 *
 * @param [in] o object
 * @param [in] ram free area for fifo
 * @param [in] size size of word of free area
 */
void fifo_mgr_init( fifo_mgr_t *o, unsigned int* ram, unsigned int size )
{
	// setting
	o->top = ram;
	o->end = ram + size;
	o->w_p = o->top;
	o->r_p = o->top;
	o->num = 0;
}

/**
 * @brief Push Entry
 *
 * @param [in] o object
 * @param [in] sensor_id sensor's ID
 * @param [in] ts timestamp
 * @param [in] p Entry
 * @param [in] size size of entry
 *
 * @return current num
 */
unsigned int fifo_mgr_push( fifo_mgr_t* o, unsigned char sensor_id, unsigned int ts, void* p, unsigned int size )
{
	unsigned int *data = ( unsigned int* )p;
	unsigned int i;
	// make free space
	while( fifo_mgr_get_free( o ) < ( 1 + 1 + size ) ) {	// header + ts + data
		if( o->num == 0 ) {				// if no more space, return num = 0;
			return ( unsigned int ) o->num;
		}
		drop( o );
	}
	// push
	*( o->w_p ) = SET_HEADER( sensor_id, size );
	o->w_p++;
	if( o->end <= o->w_p ) {
		o->w_p = o->top + ( o->w_p - o->end );
	}
	*( o->w_p ) = ts;
	o->w_p++;
	if( o->end <= o->w_p ) {
		o->w_p = o->top + ( o->w_p - o->end );
	}
	for( i = 0; i < size; i++ ) {
		*( o->w_p ) = *data;
		o->w_p++;
		data++;
		if( o->end <= o->w_p ) {
			o->w_p = o->top + ( o->w_p - o->end );
		}
	}
	o->num++;
#ifdef RUN_ON_PC
	{
		float* val = ( float* )p;
		unsigned int i;
		printf( "\t[fifo] sen_id:0x%02x", sensor_id );
		printf( ", ts:%08d", ( int )  ts );
		for( i = 0; i < size; i++ ) {
			printf( ", val[%d]:%f", ( int ) i, val[i] );
		}
		printf( "\r\n" );
	}
#endif
	return ( unsigned int ) o->num;
}

/**
 * @brief Get Entry
 *
 * @param [in] o object
 * @param [out] sensor_id sensor_id
 * @param [out] ts timestamp
 * @param [out] p entry
 * @param [out] size entry size
 *
 * @return result
 */
int fifo_mgr_peek( fifo_mgr_t* o, unsigned char *sensor_id, unsigned int *ts, void* p, unsigned int *size )
{
	return peek( o, sensor_id, ts, p, size );
}

/**
 * @brief Drop Entry
 *
 * @param [in] o object
 *
 * @return result
 */
int fifo_mgr_drop( fifo_mgr_t* o )
{
	return drop( o );
}

#ifdef _DEBUG
#include <stdio.h>
#define RAM_SIZE 16
#define PACK_SIZE 5
#define LOOP_NUM 1024
#define SENSOR_ID 0xff
static unsigned int ram[RAM_SIZE];
//int main(int argc, char const* argv[])
int test_fifo()
{
	fifo_mgr_t fifo;
	unsigned int data[PACK_SIZE];
	unsigned int ts, i, size;
	unsigned char id;
	int ret;
	printf( ">>>>>>> FIFO_MGR test start (RAM:%d, PACK:%d, ID:%d, LOOP:%d) <<<<<<<\n" , RAM_SIZE, PACK_SIZE, SENSOR_ID, LOOP_NUM );
	fifo_mgr_init( &fifo, ram, sizeof( ram ) / sizeof( int ) );
	// push phase
	for( ts = 0; ts < LOOP_NUM; ts++ ) {
		size = sizeof( data ) / sizeof( int );
		for( i = 0; i < size; i++ ) {
			data[i] = ts;
		}
		ret = fifo_mgr_push( &fifo, SENSOR_ID, ts, data, size );
		printf( "fifo_mgr_push:%d\n", ret );
	}
	// pop phase
	while( 0 < ( ret = fifo_mgr_get_num( &fifo ) ) ) {
		int c_ng = 0;
		printf( "fifo_mgr_get_num:%d\n", ret );
		ret = fifo_mgr_peek( &fifo, &id, &ts, data, &size );
		printf( "fifo_mgr_peek:%d\n", ret );
		ret = fifo_mgr_drop( &fifo );
		printf( "fifo_mgr_drop:%d\n", ret );
		for( i = 0; i < size; i++ ) {
			if( data[i] != ts ) {
				c_ng++;
			}
		}
		printf( "id:%u, ts:%u, size:%u, ng:%d\n", id, ts, size, c_ng );
	}
	printf( "fifo_mgr_get_num:%d\n", ret );
	printf( ">>>>>>> FIFO_MGR test end <<<<<<<\n" );
	return 0;
}
#endif
