/*!******************************************************************************
 * @file    frizz_const.h
 * @brief
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_CONST_H__
#define __FRIZZ_CONST_H__
#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

enum ConstIdx {
	FRIZZ_CONST_IDX_ZERO = 0,
	FRIZZ_CONST_IDX_ONE,
	FRIZZ_CONST_IDX_TWO,
	FRIZZ_CONST_IDX_HALF,
	FRIZZ_CONST_IDX_QUARTER,
	FRIZZ_CONST_IDX_THOUSAND,
	FRIZZ_CONST_IDX_360,
	FRIZZ_CONST_IDX_180,
	FRIZZ_CONST_IDX_90,
};

extern const frizz_fp *frizz_const;

/**
 * @name const table
 * @{ */
#define FRIZZ_CONST_ZERO		frizz_const[FRIZZ_CONST_IDX_ZERO]		///< =0.0f
#define FRIZZ_CONST_ONE			frizz_const[FRIZZ_CONST_IDX_ONE]		///< =1.0f
#define FRIZZ_CONST_TWO			frizz_const[FRIZZ_CONST_IDX_TWO]		///< =2.0f
#define FRIZZ_CONST_HALF		frizz_const[FRIZZ_CONST_IDX_HALF]		///< =0.5f
#define FRIZZ_CONST_QUARTER		frizz_const[FRIZZ_CONST_IDX_QUARTER]	///< =0.25
#define FRIZZ_CONST_THOUSAND	frizz_const[FRIZZ_CONST_IDX_THOUSAND]	///< =1000
#define FRIZZ_CONST_360		frizz_const[FRIZZ_CONST_IDX_360]			///< =360
#define FRIZZ_CONST_180		frizz_const[FRIZZ_CONST_IDX_180]			///< =180
#define FRIZZ_CONST_90		frizz_const[FRIZZ_CONST_IDX_90]				///< =90
/**  @} */

#ifdef __cplusplus
}
#endif

#endif//__FRIZZ_CONST_H__

