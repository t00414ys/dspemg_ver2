/*!******************************************************************************
 * @file frizz_fabs.c
 * @brief frizz_fabs() function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math_common.h"
#include "frizz_math.h"

/**
 * @brief absolute value
 *
 * @param [in] in
 *
 * @return absolute value of in
 */
frizz_fp frizz_fabs( frizz_fp in )
{
	_MathFPCast *p = ( _MathFPCast* )&in;
	p->fbit.sign = 0;
	return p->zp;
}

