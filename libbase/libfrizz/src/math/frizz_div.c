/*!******************************************************************************
 * @file frizz_div.c
 * @brief frizz_div() function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math_common.h"
#include "frizz_math.h"

/**
 * @brief constant value for frizz_div
 */
static const float div_r[] = {
	// y = a * x + b
	-0.5,
	1.4571067811865475244008443621048,
	// A(n+1) = A(n) * (2.0 - x * A(n))
	2.0,
};

/**
 * @brief divide opreation
 *
 * @param [in] xin
 * @param [in] yin
 *
 * @return value of xin/yin
 */
frizz_fp frizz_div( frizz_fp xin, frizz_fp yin )
{
	_MathFPCast x, y, f;
	const frizz_fp *r = ( const frizz_fp* )div_r;
	int i, exp;

	if( yin == CONST_ZERO ) {
		f.zp = yin;
		f.fbit.exp = 255;
		f.fbit.frac = 0x400000;
		return f.zp;
	} else if( xin == CONST_ZERO ) {
		return CONST_ZERO;
	}

	x.zp = xin;
	y.zp = yin;
	i = y.fbit.sign;
	exp = y.fbit.exp;
	if( exp == 0 ) {
		for( exp = 1; y.fbit.exp == 0; y.ui <<= 1, exp-- );
	}
	exp -= 127;
	y.fbit.exp = 127;
	y.fbit.sign = 0;

	f.zp = r[0] * y.zp + r[1];

	y.fbit.sign = i;
	f.fbit.sign = i;
	for( i = 0; i < 3; i++ ) {
		// A(n+1) = A(n) * (2.0 - x * A(n)) for 1/x
		f.zp = f.zp * ( r[2] - y.zp * f.zp );
	}

	{
		int e;
		e = x.fbit.exp;
		if( e == 0 ) {
			i = x.fbit.sign;
			for( e = 1; x.fbit.exp == 0; x.ui <<= 1, e-- );
			x.fbit.sign = i;
		}
		e -= 127;
		x.fbit.exp = 127;

		exp = e - exp;
	}

	f.zp = x.zp * f.zp;
	exp = f.fbit.exp + exp;
	if( 254 < exp ) {
		f.fbit.exp = 255;
		f.fbit.frac = 0;
	} else if( 0 < exp ) {
		f.fbit.exp = exp;
	} else {
		unsigned int shift = 31 < ( 1 - exp ) ? 31 : ( 1 - exp );
		/* subnominal */
		f.fbit.frac = ( f.fbit.frac | 0x800000 ) >> shift;
		f.fbit.exp = 0;
	}

	return f.zp;
}

