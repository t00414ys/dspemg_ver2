/*!******************************************************************************
 * @file    version.h
 * @brief
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __VERSION_H__
#define __VERSION_H__
#include "make_time.h"


#define	FRIZZ_VERSION	"$$$$ "TO_STRING(FRIZZ_VER_MAJOR)"."TO_STRING(FRIZZ_VER_MINOR)"."TO_STRING(FRIZZ_VER_DETAIL)"  : "MAKEDATE" $$$$"

#endif//__VERSION_H__

